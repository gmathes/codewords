from django.db import models


class Game(models.Model):
    name = models.CharField(max_length=254)
    active = models.BooleanField(default=True)

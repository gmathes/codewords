from django.views.generic import ListView, DetailView, TemplateView, UpdateView, DeleteView, CreateView, View
from django.urls import reverse_lazy

from .models import Game


class LobbyView(ListView):
    model = Game
    context_object_name = 'games'


class GameCreate(CreateView):
    model = Game
    fields = ['name']
    success_url = reverse_lazy('home')


class GameDetail(DetailView):
    model = Game

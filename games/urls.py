from django.urls import path

from . import views

urlpatterns = [
    #path('', views.AgencyIndex.as_view(), name='game.index'),
    path('create/', views.GameCreate.as_view(), name='game.create'),
    path('detail/<pk>/', views.GameDetail.as_view(), name='game.detail'),
]
